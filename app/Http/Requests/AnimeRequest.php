<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnimeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3',
            //'author' => 'required|max:50',
            'description' => 'required|max:100',
            'episodes' => 'required|numeric',
            'img' => 'required|image',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => "Scrivi il titolo!",
            //'author.required' => "Scrivi l'autore!",
            'description.required' => "Inserisci il plot!",
            'episodes.required' => "inserisci il numero di episodi!",
            'title.min' => "scrivi almeno 3 caratteri",
            //'author.max' => "Nome di massimo 50 caraeri",
            'description.max' => "la descrizione deve avere massimo 100",
            'episodes.numeric' => "solo numeri nel campo episodi",
            'img.required' => "Ricordati di mettere la copertina",
            'img.image' => "Solo file jpg, jpeg",
        ];
    }
}
