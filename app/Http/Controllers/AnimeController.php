<?php

namespace App\Http\Controllers;

use App\Models\Platform;
use App\Http\Requests\AnimeRequest;
use App\Models\Anime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnimeController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth")->except("indexAnime");
    }


    public function formAnime(){
        $platforms = Platform::all();
        return view("anime/formAnime", compact("platforms"));
    }


    public function submitAnime(AnimeRequest $request){
        //dd($request->all());

        //mass assignament
        
        $anime = Auth::user()->animes()->create([
            'title'=>$request->input("title"),
            'description'=>$request->input("description"),
            //'author'=>$user->name,
            'episodes'=>$request->input("episodes"),
            'img'=>$request->file('img')->store("public/img"),
        ]);

// fuzione per attacare in modo multiplo i mio cartone a piùpiattafore

        foreach($request->platforms as $platform){
            if($platform){
                $anime->platforms()->attach($platform);
            }
        }

        $anime->platforms()->attach($request->platform);



            foreach($request -> platforms as $platform){
                if($platform){
                    $anime->platforms()->attach($platform);
                }
            }


        // $anime = Anime::create(
        //     [
        //         'title'=>$request->input("title"),
        //         'description'=>$request->input("description"),
        //         'author'=>$request->input("author"),
        //         'episodes'=>$request->input("episodes"),
        //         'img'=>$request->file('img')->store("public/img"),
        //     ]
        // );

        return redirect(route("indexAnime"));

        // //metodo normale
        // $anime = new Anime();
        // $anime->title = $request->input("title");
        // $anime->description = $request->input("description");
        // $anime->author = $request->input("author");
        // $anime->episodes = $request->input("episodes");
        // //dd($anime);
        // $anime->save();
    }

    public function indexAnime(){
        $animes = Anime::all();
        return view("anime.indexAnime", compact("animes"));
    }

    // public function detailAnime($anime){
    //     $cartoon = Anime::find($anime);
    //     return view("anime.detailAnime", compact("cartoon"));
    // }

    // con OOP
    public function detailAnime(Anime $anime){
        return view("anime.detailAnime", compact("anime"));
    }

    //vista per l form di modifica
    public function editAnime(Anime $anime){
        return view("anime.editAnime", compact("anime"));
    }

    //funzione per modifica anime
    public function updateAnime(Request $req, Anime $anime){
        //dd($req, $anime);
        $anime->title = $req->title;
        $anime->author = $req->author;
        $anime->description = $req->description;
        $anime->episodes = $req->episodes;
        $anime->img = $req->file("img")->store("public/img");
        //dd($anime);
        $anime->save();
        return redirect(route("welcome"));
    }

    //funzione per la cancellazzione dl record anime selezionato
    public function destroyAnime(Anime $anime){
        $anime->delete();
        return redirect(route("welcome"));
    }


    public function userAnime(){
        $animes = Auth::user()->animes()->get();
        return view("anime.userAnime", compact("animes"));
    }
}