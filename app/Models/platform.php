<?php

namespace App\Models;

use App\Models\Anime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Platform extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];


    //funzione di relazione che collega il modello Platform al modello Anime
    public function animes(){
        return $this->belongsToMany(Anime::class);
    }
}
