<?php

namespace App\Models;

use App\Models\User;
use App\Models\Platform;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Anime extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'episodes',
        //'author',
        'img',
    ];

    //Funzione di relazione che collega i modllo Anime allo user
    public function user(){
        return $this->belongsTo(User::class);
    }

    //funzione di relazione che collega il modello Anime al modello Platform
    public function platforms(){
        return $this->belongsToMany(Platform::class);
    }
}
