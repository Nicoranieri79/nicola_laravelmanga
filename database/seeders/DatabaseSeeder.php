<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $platforms = [
            ['name' => 'Netflix'],
            ['name' => 'Amazon'],
            ['name' => 'AppleTv'],
            ['name' => 'Sky'],
        ];

        foreach($platforms as $platform){
            DB::table('platforms')->insert([
                'name' => $platform['name'],
                //uso della libreria Carbon
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

        // DB::table('platforms')->insert([
        //     'name' => str::random(10),
        // ]);


    }
}
