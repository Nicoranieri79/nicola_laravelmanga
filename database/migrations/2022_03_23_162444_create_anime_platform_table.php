<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anime_platform', function (Blueprint $table) {
            $table->id();
            //coonna con  riferimenti alle PK della colonna animes
            $table->unsignedBigInteger("anime_id");
            //vincolo di itegrità referenziale per la colonna anime_id
            $table->foreign("anime_id")->references("id")->on("animes");
            //colonna coi riferimenti alle PK della colonna platforms
            $table->unsignedBigInteger("platform_id");
            //vincolo di integrità referenziale per la colonna platforms_id su platforms
            $table->foreign("platform_id")->references("id")->on("platforms");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anime_platform');
    }
};
