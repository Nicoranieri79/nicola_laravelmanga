<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('animes', function (Blueprint $table) {
            //SEAGGIO DELLA COLONNA
            $table->unsignedBigInteger("user_id")->after("author")->default(1);
            //SETTAGGIO dl Vinco di Integrità Differenziale 
            $table->foreign("user_id")->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('animes', function (Blueprint $table) {
            //drop del vincolo
            $table->dropForeign(["user_id"]);
            //drop della colonna
            $table->dropColumn("user_id");
        });
    }
};
