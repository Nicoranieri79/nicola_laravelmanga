<?php

use App\Http\Controllers\AnimeController;
use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;


Route::get('/', [PublicController::class, "welcome"])->name("welcome");

// ROTTE PER GLI ANIME
Route::get("/form/anime", [AnimeController::class, "formAnime"])->name("formAnime");
Route::post("/form/anime/submit", [AnimeController::class , "submitAnime"])->name("submitAnime");
Route::get("/anime/index", [AnimeController::class, "indexAnime"])->name("indexAnime");
Route::get("/anime/index/{anime}", [AnimeController::class, "detailAnime"])->name("detailAnime");
Route::get("/anime/edit/{anime}", [AnimeController::class, "editAnime"])->name("editAnime");
Route::put("/anime/update/anime", [AnimeController::class, "updateAnime"])->name("updateAnime");
Route::delete("/anime/destroy/{anime}", [AnimeController::class, "destroyAnime"])->name("destroyAnime");
Route::get("/anime/useranime", [AnimeController::class, "userAnime"])->name("userAnime");