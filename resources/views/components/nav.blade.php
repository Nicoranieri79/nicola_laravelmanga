<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{route("welcome")}}">Welcome</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <div class="navbar-nav">
            <a class="nav-link active" aria-current="page" href="{{route("welcome")}}">Welcome</a>
            <a class="nav-link" href="{{route("formAnime")}}">Inserisci Anime</a>
            <a class="nav-link" href="{{route("userAnime")}}">I tuoi Anime</a>
            <a class="nav-link" href="{{route("indexAnime")}}">I nostri Anime</a>
          @guest
            <a class="nav-link" href="{{route("register")}}">Registrati</a>
            <a class="nav-link" href="{{route("login")}}">Accedi</a>
          @else
            <a class="nav-link" href="">{{Auth::user()->name}}</a>
            <a class="nav-link" href="{{ route('logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
              @csrf
            </form>
            @endguest
        </div>
      </div>
    </div>
  </nav>