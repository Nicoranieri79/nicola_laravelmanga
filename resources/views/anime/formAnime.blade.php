<x-layout>
    <div class="container">
        <div class="justify-content-center">
            <div class="col-12 col-md-6">
                <h1>
                    inserisci il tuo Anime!
                </h1>
                <h1>Create Post</h1>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="POST" action="{{route("submitAnime")}}" enctype="multipart/form-data">
                  @csrf
                    <div class="mb-3">
                      <label for="exampleInputTitle" class="form-label">Inserisci il Titolo</label>
                      <input type="text" class="form-control" name="title" value="{{old("title")}}">
                    </div>

                    {{-- <div class="mb-3">
                      <label for="exampleInputAuthor" class="form-label">Inserisci il tuo nome</label>
                      <input type="text" class="form-control" name="author" value="{{old("author")}}">
                    </div> --}}

                    <div class="mb-3">
                      <label for="exampleInputDescription" class="form-label">Inserisci la trama</label>
                      <input type="text" class="form-control" name="description" value="{{old("description")}}">
                    </div>

                    <div class="mb-3">
                      <label for="exampleInputEpisodes" class="form-label">Numero episodi</label>
                      <input type="text" class="form-control" name="episodes" value="{{old("episodes")}}">
                    </div>

                    <div class=" card-text mb-3">
                      <h3>Dove vederlo</h3>
                      @foreach ($anime->platforms as $platform)
                      <ul><li>{{$platform->name}}</li></ul>
                      @endforeach
                    </div>

                    <div class="mb-3">
                      <select name="platforms[]" multiple>
                        @foreach ($platforms as $platform)
                          <option value="{{$platform->id}}">
                            {{$platform->name}}
                          </option>
                        @endforeach
                      </select>
                    </div>

                    <div class="mb-3">
                      <p>Inserisci la copertina</p>
                      <input type="file" name="img">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
            </div>
        </div>
    </div>
</x-layout>