<x-layout>
    <div class="container">
        <div class="row">
                <h1>
                    I nostri Anime!
                </h1>
                @foreach ($animes as $anime)
                    <div class="card mx-5 my-5" style="width: 18rem;">
                        {{-- @if ($anime->img)
                        <img src="{{Storage::url($anime->img)}}" class="card-img-top" alt="...">
                        @else
                        <img src="https://picsum.photos/id/237/200/300" alt="">
                        @endif --}}
                        <img src="{{Storage::url($anime->img)}}" class="card-img-top" alt="...">

                        <div class="card-body">
                        <h5 class="card-title">{{$anime->title}}</h5>
                        <p class="card-text">Trama</p>
                        <a href="{{route("detailAnime" , compact("anime") )}}" class="btn btn-primary">Dettaglio</a>
                        </div>
                    </div>
                @endforeach
        </div>
    </div>
</x-layout>