<x-layout>
    <h1>
        I tuoi cartoni
    </h1>

    <div class="row col-12 mb-4">
        @foreach ($animes as $anime)

        <div class="card my-3 mx-3" style="width: 18rem;">
            <img src="{{Storage::url($anime->img)}}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Autore: {{$userAnime->user->name}}</h5>
                <h5 class="card-title" class="card-text">Trama</h5>
                <p>{{$anime->description}}</p>
            </div>
        </div>
        @endforeach
    </div>
</x-layout>