<x-layout>
    <div class="container">
        <div class="justify-content-center">
            <div class="col-12 col-md-6">
                <h1>
                    Modifica il tuo Anime!
                </h1>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="POST" action="{{route("updateAnime", compact("anime"))}}" enctype="multipart/form-data">
                  @csrf
                  @method("put")
                    <div class="mb-3">
                      <label for="exampleInputTitle" class="form-label">Inserisci il Titolo</label>
                      <input type="text" class="form-control" name="title" value="{{$anime->title}}">
                    </div>

                    <div class="mb-3">
                      <label for="exampleInputAuthor" class="form-label">Inserisci il tuo nome</label>
                      <input type="text" class="form-control" name="author" value="{{$anime->author}}">
                    </div>

                    <div class="mb-3">
                      <label for="exampleInputDescription" class="form-label">Inserisci la trama</label>
                      <input type="text" class="form-control" name="description" value="{{$anime->description}}">
                    </div>

                    <div class="mb-3">
                      <label for="exampleInputEpisodes" class="form-label">Numero episodi</label>
                      <input type="text" class="form-control" name="episodes" value="{{$anime->episodes}}">
                    </div>

                    <div class="mb-3">
                      <p>Modifica la copertina</p>
                      <img src="{{Storage::url($anime->img)}}">
                      <input type="file" name="img">
                    </div>
                    
                    <button type="submit" class="btn btn-warning">Modifica</button>
                  </form>
            </div>
        </div>
    </div>
</x-layout>