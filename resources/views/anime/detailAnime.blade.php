<x-layout>
    <h1>
        Dettaglio del Cartone
    </h1>
    <div class="card mx-5" style="width: 18rem;">
        @if ($anime->img)
                <img src="{{Storage::url($anime->img)}}" class="card-img-top" alt="...">
            @else
                <img src="https://picsum.photos/id/237/200/300" alt="">
         @endif
        <div class="card-body">
            <h5>Titolo:</h5>
            <h5 class="card-title">{{$anime->title}}</h5>
            <p>Descrizione:</p>
            <p class="card-text">{{$anime->description}}</p>
            <h5>Autore: {{$anime->user->name}}</h5>
            
            <p>Episodi:</p>
            <p class="card-text">{{$anime->episodes}}</p>
            
            <form class="d-flex justify-content-around" method="POST" action="{{route("destroyAnime", compact("anime"))}}">
                @csrf
                @method("delete")
                <button type="submit" class="btn btn-danger">Cancella</button>
                <button class="btn btn-warning" href="{{route("editAnime", compact("anime"))}}">Modifica</button>
            </form>
        </div>
    </div>
</x-layout>